package org.saintgrall;

import jakarta.persistence.*;
import java.util.*;

import org.saintgrall.models.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("-------------------------- Start -------------------------");
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("test");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();

        Address address = new Address("69", "rue des coiffeurs", "69420", "Condrieu");
        Address address2 = new Address("21", "place sigma", "42830", "La Tuilière");
        Address address3 = new Address("12", "Avenue de la Marne", "56100", "Lorient");
        em.persist(address);
        em.persist(address2);
        em.persist(address3);

        Product product1 = new Product("QUOIF2R", "Pousse Pousse les poils de ton chat", ProductType.CLEANING, 69.99);
        Product product2 = new Product("PETIT268F", "Croquettes Grostas", ProductType.FOOD, 27.99);
        Product product3 = new Product("HNLJ3945", "Heinkel He 178", ProductType.ACCESSORY, 3945.99);
        em.persist(product1);
        em.persist(product2);
        em.persist(product3);

        Set<Product> productSet = new HashSet<>();
        productSet.add(product1);
        productSet.add(product2);
        productSet.add(product3);

        PetStore petStore = new PetStore("Nos chats sont nos amis", "Thomas Grall", productSet, address);
        PetStore petStore2 = new PetStore("Pierce & Pierce", "Patrick Bateman", productSet, address2);
        PetStore petStore3 = new PetStore("Gold's Gym", "Ernest Khalimov", productSet, address3);
        em.persist(petStore);
        em.persist(petStore2);
        em.persist(petStore3);

        Fish fish1 = new Fish(new Date(2023, 10, 12), "Taupe", petStore, FishLivingEnvironment.FRESH_WATER);
        Fish fish2 = new Fish(new Date(2022, 11, 12), "Blanc cassé Gris ascendant Jaune",petStore2,  FishLivingEnvironment.SEA_WATER);
        Fish fish3 = new Fish(new Date(2023, 3, 2), "Transparent",petStore3, FishLivingEnvironment.FRESH_WATER);
        em.persist(fish1);
        em.persist(fish2);
        em.persist(fish3);


        Cat cat1 = new Cat(new Date(1969, 12, 21), "Noir", petStore, "MCKLBLC69");
        Cat cat2 = new Cat(new Date(2002, 8, 4), "Bleu ciel", petStore2, "TGRL02");
        Cat cat3 = new Cat(new Date(2001, 9, 11), "Tourmaline", petStore3, "TRJMLL01");
        em.persist(cat1);
        em.persist(cat2);
        em.persist(cat3);

        em.getTransaction().commit();
        TypedQuery<Animal> findAllQUery = em.createQuery("from animal where petStores='3'", Animal.class);
        System.out.println(findAllQUery.getResultList());

        em.close();
        emf.close();
    }
}